package com.epam.utils;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class WebDriverManagerTest {

    @Test
    public void testGetDriver() {
        assertNotNull(WebDriverManager.getDriver());
    }

    @Test
    public void testQuit() {
        WebDriver driver = WebDriverManager.getDriver();
        WebDriverManager.quit();
        assertEquals(driver.toString(), "ChromeDriver: chrome on XP (null)");
    }
}