package com.epam;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.testng.Assert.*;

public class GooglePageTest {
    private static final Logger LOGGER = LogManager.getLogger(GooglePage.class);
    private static GooglePage page;

    @BeforeTest
    public static void navigate(){
        page = new GooglePage();
        page.navigate();
    }

    @Test(dataProvider = "values")
    public void testGoogle(String toSearch) {
        page.clearInput();
        assertTrue(page.google(toSearch).startsWith(toSearch));
    }

    @Test(dataProvider = "values")
    public void testGetPhotos(String toSearch) {
        page.clearInput();
        page.google(toSearch);
        assertNotNull(page.getPhotos());
    }

    @AfterTest
    public static void quit(){
        page.quit();
    }

    @DataProvider(name = "values")
    public static Object[][] getData() {
        return readFromCSV(GooglePage.class.getResource("/values.csv").getPath(), ",");
    }

    private static String[][] readFromCSV(String csvPath, String delimiter) {
        List<String[]> data = new ArrayList<>();
        String line = "";
        try (BufferedReader br = new BufferedReader(new FileReader(csvPath))) {
            while ((line = br.readLine()) != null) {
                data.add(line.split(delimiter));
            }
        } catch (IOException e) {
            LOGGER.error("Could not read from CSV file!");
            e.printStackTrace();
        }
        return data.toArray(new String[data.size()][]);
    }
}