package com.epam;

import com.epam.utils.WebDriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class GooglePage {
    private static final Logger LOGGER = LogManager.getLogger(GooglePage.class);

    private static WebDriver driver;

    public GooglePage() {
        driver = WebDriverManager.getDriver();
    }

    public void navigate() {
        driver.get("https://www.google.com");
    }

    public String google(String input) {
        WebElement search = driver.findElement(By.name("q"));
        search.sendKeys(input);
        search.submit();
        return driver.getTitle();
    }

    public void clearInput(){
        driver.findElement(By.name("q")).clear();
    }

    public List<WebElement> getPhotos() {
        WebElement photos = driver.findElement(By.xpath("//*[@id=\"hdtb-msb-vis\"]/div[2]"));
        photos.click();
        List<WebElement> photosList = driver.findElements(By.cssSelector("#irc-ss > div:nth-child(3) "
                + "> div.irc_t.i30052 > div.irc_mic > div.irc_mimg.irc_hic > a > div > img"));
        return photosList;
    }

    public void quit() {
        WebDriverManager.quit();
    }

    public static void main(String[] args) {
        GooglePage page = new GooglePage();
        page.navigate();
        page.google("Apple");
        page.getPhotos();
        page.quit();
    }

}
